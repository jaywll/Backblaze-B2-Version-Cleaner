<?php
	error_reporting(E_ERROR);
	
	$cmdline = getopt("d:b:");	
	if (!$cmdline || !is_numeric($cmdline['d'])) die("Failure: You must specify a bucket name and a number of days as command line arguments -b and -d respectively.\n");
	
	$minsafeage = (time() - ($cmdline['d'] * 86400)) * 1000;

	$b2account = file_get_contents($_SERVER['HOME'].'/.b2_account_info');
	$b2account = json_decode($b2account, true);
	
	if (!isset($b2account['bucket_names_to_ids'][$cmdline['b']])) die("Failure: Bucket name not found in ".$_SERVER['HOME']."/.b2_account_info\n");

	$url = $b2account['api_url'].'/b2api/v1/b2_list_file_versions';

	$data = array(
		nextFileName => false,
		nextFileId => false
	);

	do {
		$parameters = array(
			'bucketId' => $b2account['bucket_names_to_ids'][$cmdline['b']],
			'maxFileCount' => 1000,
			'startFileName' => $data['nextFileName'],
			'startFileId' => $data['nextFileId'],
		);

		if ($data['nextFileName'] === false) {
			unset($parameters['startFileName'], $parameters['startFileId']);
		}

		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Authorization: '.$b2account['account_auth_token'],
				'content' => json_encode($parameters),
				'ignore_errors' => true
			)
		);

		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$data = json_decode($data, true);

		foreach ($data['files'] as $file) {
			$bucketfiles[$file['fileName']][$file['uploadTimestamp']]['action'] = $file['action'];
			$bucketfiles[$file['fileName']][$file['uploadTimestamp']]['fileId'] = $file['fileId'];
		}
		echo "Preparing to clean:    Fetched details for ".count($bucketfiles)." file(s).\n";
	} while ($data['nextFileId'] != '');
	
	echo "Preparing to clean:    All file version information fetched from bucket.\n";

	foreach($bucketfiles as $filename => $versions) {
		if (count($versions) == 1) unset($bucketfiles[$filename]);
		else {
			krsort($versions);
			$deleteflag = false;

			foreach($versions as $timestamp => $version) {
				if (!$deleteflag) {
					if ($timestamp < $minsafeage) {
						$deleteflag = true;

						if ($version['action'] == 'hide') {
							$deletelist[] = array(
								'fileName' => $filename,
								'fileId' => $version['fileId']
							);
						}
					}
				} else {
					$deletelist[] = array(
						'fileName' => $filename,
						'fileId' => $version['fileId']
					);
				}
			}
		}
	}

	if (count($deletelist) == 0) exit("Nothing to clean.\n");

	$url = $b2account['api_url'].'/b2api/v1/b2_delete_file_version';

	foreach($deletelist as $deleteitem) {
		echo "Cleaning:              Deleting old version of ".$deleteitem['fileName']."\n";
		$parameters = array(
			'fileId' => $deleteitem['fileId'],
			'fileName' => $deleteitem['fileName']
		);

		$options = array(
			'http' => array(
				'method' => 'POST',
				'header' => 'Authorization: '.$b2account['account_auth_token'],
				'content' => json_encode($parameters),
				'ignore_errors' => true
			)
		);

		$context = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
	}
?>
