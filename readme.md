# Backblaze B2 Version Cleaner #
Backblaze B2 Version Cleaner is a PHP script designed to be run from the command line, possibly (but not necessarily) as part of a larger bash backup script.

Backblaze B2 is a cloud storage platform similar to Amazon S3, Google Cloud Storage, *et al*. In contrast to these other services it retains an unlimited history of file versions: when a file is updated, old versions are kept (and continue to use storage space).

This script removes old versions once the current version reaches a certain age (in days).

## Usage &amp; Setup ##
Run the script from the command line, and pass in the bucket name and number of days old a file version must be before it's considered safe to remove previous versions.

`php /path/cleanup.php -b bucketname -d 5`

The script retrieves account details (including the ID of the named bucket) from the file `~/b2_account_info`. This file is created when Backblaze's own command line tool is used, so in order for the script to function this tool must have been used recently to perform an operation on the named bucket.